#include<windows.h>
#include<wininet.h>
#include<iostream>
using namespace std;
#pragma comment(lib,"wininet.lib")
int main()
{
	DWORD byteread = 0;//下载字节数 DWORD 就是 Double Word， 每个word为2个字节的长度，DWORD 双字即为4个字节，每个字节是8位，共32位。
	char buffer[100] ;//缓冲区
	memset(buffer, 0, 100);//内存初始化
	HINTERNET internetopen;//winhttp句柄
	internetopen = InternetOpen("Testing", INTERNET_OPEN_TYPE_PRECONFIG, NULL, NULL, 0);//InternetOpen初始化wininet程序，INTERNET_OPEN_TYPE_PRECONFIG 返回注册表中代理或直接的配置；
	if (internetopen==NULL)
	{
		cout << "网络连接打开失败" << endl;
		return 0;
	}
	HINTERNET internetopenurl;
	//InternetOpenUrl中打开由完整FTP或HTTP URL指定的资源。
	internetopenurl = InternetOpenUrl(internetopen, "https://dl.softmgr.qq.com/original/game/QMProxyAcc_Setup_3.0.4683.134_2000.exe", NULL, 0, INTERNET_FLAG_RELOAD, 0);
	//INTERNET_FLAG_RELOAD从原服务器强制下载所要求的文件，对象，或目录列表，而不是从缓存下载。
	if (internetopenurl==NULL)
	{
		cout << "资源不存在或链接无法打开" << endl;
		InternetCloseHandle(internetopen);
		return 0;
	}
	BOOL hwriter;
	DWORD written;
	HANDLE createfile;
	createfile = CreateFile("d://sss.exe", GENERIC_WRITE, 0, 0, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
	//如果为 GENERIC_WRITE 表示允许对设备进行写访问（可组合使用）；
	//CREATE_ALWAYS 创建文件，会改写前一个文件
	//FILE_ATTRIBUTE_NORMAL 默认属性
	if (createfile==INVALID_HANDLE_VALUE)//INVALID_HANDLE_VALUE表示无效句柄
	{
		cout << "文件创建失败" << endl;
		InternetCloseHandle(internetopenurl);
		return 0;
	}
	BOOL internetreadfile;
	while (true)
	{
		internetreadfile = InternetReadFile(internetopenurl, buffer, sizeof(buffer), &byteread);
		if (byteread == 0)
		{
			break;
		}
		hwriter = WriteFile(createfile, buffer, sizeof(buffer), &byteread,NULL);
		if (hwriter==0)
		{
			cout << "写入文件时失败" << endl;
			CloseHandle(createfile);
			return 0;
		}
	
	}
	cout << "下载成功" << endl;
	return 0;

}